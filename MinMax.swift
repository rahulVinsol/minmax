func minMax(_ num1: Int, _ num2: Int) -> (Int, Int) {
	num1 < num2 ? (num1, num2) : (num2, num1)
}

print(minMax(2, 5))
print(minMax(5, 2))
